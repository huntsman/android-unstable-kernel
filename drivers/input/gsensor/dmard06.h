#ifndef DMARD06_H
#define DMARD06_H

#include <linux/ioctl.h>

#define DMARD06_DEVID				0x0
#define RANGE			2000000

#define RBUFF_SIZE		12	/* Rx buffer size */

#define MMAIO				0xA1

/* IOCTLs for MMA8452 library */
#define MMA_IOCTL_INIT                  _IO(MMAIO, 0x01)
#define MMA_IOCTL_RESET      	          _IO(MMAIO, 0x04)
#define MMA_IOCTL_CLOSE		           _IO(MMAIO, 0x02)
#define MMA_IOCTL_START		             _IO(MMAIO, 0x03)
#define MMA_IOCTL_GETDATA               _IOR(MMAIO, 0x08, char[RBUFF_SIZE+1])

/* IOCTLs for APPs */
#define MMA_IOCTL_APP_SET_RATE		_IOW(MMAIO, 0x10, char)

//Registers
#define DMARD06_REG_WHO_AM_I                    0x0f
#define DMARD06_REG_WHO_AM_I_VALUE 	        0x06
#define DMARD06_REG_X_OUT                       0x41
#define DMARD06_REG_NORMAL                      0x44     //???
#define DMARD06_REG_MODE 	                0x45     //???
#define DMARD06_REG_FLITER 	                0x46     //???
#define DMARD06_REG_NA 	                        0x48     //???
#define DMARD06_REG_EVENT                       0x4A     //???
#define DMARD06_REG_Threshold                   0x4C     //???
#define DMARD06_REG_Duration                    0x4D     //???
#define DMARD06_REG_INT 	                0x47     //???
#define SW_RESET 	                        0x53

#define DMARD06_RATE_SHIFT		  5
//#define DMARD06_RATE_50			4
#define DMARD06_RATE_2          6
#define DMARD06_RATE_4          5
#define DMARD06_RATE_8          4
#define DMARD06_RATE_16         3
#define DMARD06_RATE_32         2
#define DMARD06_RATE_NORMAL     1


#define DMARD06_PRECISION       7
#define DMARD06_BOUNDARY        (0x1 << (DMARD06_PRECISION - 1))
#define DMARD06_GRAVITY_STEP    RANGE / DMARD06_BOUNDARY

/*status*/
#define DMARD06_SUSPEND           2
#define DMARD06_OPEN           1
#define DMARD06_CLOSE          0

struct dmard06_data {
        struct mutex lock;
	struct i2c_client *client;
	//struct work_struct work;
	struct delayed_work delaywork;
	struct input_dev *input_dev;
        int status;
        int delay;
        char  curr_tate;
        struct hrtimer timer;
        struct workqueue_struct *dmard06_wq;
};

struct dmard06_axis {
	int x;
	int y;
	int z;
};

#endif