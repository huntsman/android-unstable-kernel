#ifndef DMARD08_H
#define DMARD08_H

#include <linux/ioctl.h>

#define DMARD08_DEVID				0x08
#define RANGE			4000000

#define RBUFF_SIZE		12	/* Rx buffer size */

#define MMAIO				0xA1

/* IOCTLs for MMA8452 library */
#define MMA_IOCTL_INIT                  _IO(MMAIO, 0x01)
#define MMA_IOCTL_RESET      	          _IO(MMAIO, 0x04)
#define MMA_IOCTL_CLOSE		           _IO(MMAIO, 0x02)
#define MMA_IOCTL_START		             _IO(MMAIO, 0x03)
#define MMA_IOCTL_GETDATA               _IOR(MMAIO, 0x08, char[RBUFF_SIZE+1])

/* IOCTLs for APPs */
#define MMA_IOCTL_APP_SET_RATE		_IOW(MMAIO, 0x10, char)

//Registers
#define DMARD08_REG_X_OUT			0x02



#define DMARD08_PRECISION       11
#define DMARD08_BOUNDARY        (0x1 << (DMARD08_PRECISION - 1))
#define DMARD08_GRAVITY_STEP    RANGE / DMARD08_BOUNDARY

/*status*/
#define DMARD08_SUSPEND           2
#define DMARD08_OPEN           1
#define DMARD08_CLOSE          0

struct dmard08_data {
        int delay;
	struct i2c_client *client;
	struct input_dev *input_dev;
	struct delayed_work delaywork;
};

struct dmard08_axis {
	int x;
	int y;
	int z;
};

#endif