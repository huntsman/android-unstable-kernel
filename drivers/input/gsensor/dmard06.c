
 /****************************************************************************************
 * File:			driver/input/gsensor/lis3dh.c
 * Copyright:		Copyright (C) 2012-2013 RK Corporation.
 * Author:		LiBing <libing@rock-chips.com>
 * Date:			2012.03.06
 * Description:	This driver use for rk29 chip extern gsensor. Use i2c IF ,the chip is 
 * 				STMicroelectronics lis3dh.
 *****************************************************************************************/
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/miscdevice.h>
#include <linux/gpio.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/freezer.h>
#include <mach/gpio.h>
#include <mach/board.h> 
#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

#include "dmard06.h"


#if 1
#define PRINT_INFO(x...) printk(x)
#else
#define PRINT_INFO(x...)
#endif

#define DMARD06_SPEED		200 * 1000

#define MAX_DELAY	200
#define DEF_DELAY	30


static struct i2c_client *this_client;
static struct miscdevice dmard06_device;
static struct kobject *android_gsensor_kobj;
static const char* vendor = "DMARD06 GS-SENSOR";
struct dmard06_axis *spec_axis;
#ifdef CONFIG_HAS_EARLYSUSPEND
static struct early_suspend dmard06_early_suspend;
#endif
static int  dmard06_probe(struct i2c_client *client, const struct i2c_device_id *id);
static DECLARE_WAIT_QUEUE_HEAD(data_ready_wq);

/* AKM HW info */
static ssize_t gsensor_vendor_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	ssize_t ret = 0;

	//sprintf(buf, "%#x\n", revision);
	 sprintf(buf, "%s.\n", vendor);
	ret = strlen(buf) + 1;

	return ret;
}

static DEVICE_ATTR(vendor, 0444, gsensor_vendor_show, NULL);

static int gsensor_sysfs_init(void)
{
	int ret ;

	android_gsensor_kobj = kobject_create_and_add("android_gsensor", NULL);
	if (android_gsensor_kobj == NULL) {
		PRINT_INFO(KERN_ERR
			   "DMARD06 gsensor_sysfs_init:"\
			   "subsystem_register failed\n");
		ret = -ENOMEM;
		goto err;
	}

	ret = sysfs_create_file(android_gsensor_kobj, &dev_attr_vendor.attr);
	if (ret) {
		PRINT_INFO(KERN_ERR
			   "DMARD06 gsensor_sysfs_init:"\
			   "sysfs_create_group failed\n");
		goto err4;
	}

	return 0 ;
err4:
	kobject_del(android_gsensor_kobj);
err:
	return ret ;
}

static int dmard06_rx_data(struct i2c_client *client, char *rxData, int length)
{
	int liRet = 0;
	char lcReg = rxData[0];
	liRet = i2c_master_reg8_recv(client, lcReg, rxData, length, DMARD06_SPEED);
	
	return (liRet > 0)? 0 : liRet;
}

static int dmard06_tx_data(struct i2c_client *client, char *txData, int length)
{
	int liRet	= 0;
	char lcReg	= txData[0];
	liRet = i2c_master_reg8_send(client, lcReg, &txData[1], length-1, DMARD06_SPEED);
	
	return (liRet > 0)? 0 : liRet;
}

static int dmard06_set_rate(struct i2c_client *client, char rate)
{

	int ret = 0;
	char buffer[2];
	if (rate > 3)
		return -EINVAL;
	PRINT_INFO("[ZWP]%s,rate = %d\n",__FUNCTION__,rate);
	//?????????,??RawDataLength??????,????gsensor????

	PRINT_INFO("[ZWP]%s,new rate = %d\n",__FUNCTION__,rate);


	buffer[0] = DMARD06_REG_NORMAL;
	//buffer[1] = (rate<< DMARD06_RATE_SHIFT) | 0x07;
        buffer[1] = 0x27 | (0x18 & (rate<< DMARD06_RATE_SHIFT));

	ret = dmard06_tx_data(client, &(buffer[0]), 2);
	ret = dmard06_rx_data(client, &(buffer[0]), 1);

	return ret;
}

static int dmard06_start_dev(struct i2c_client *client, char rate)
{
      struct dmard06_data *dmard06;

	char buffer[4];

	int ret = 0;
	dmard06 = i2c_get_clientdata(client);
       buffer[0] = DMARD06_REG_NORMAL;
	buffer[1] = 0x27;	//0x10; modify by zhao
       ret = dmard06_tx_data(client, &buffer[0], 2);
	 buffer[0] = DMARD06_REG_MODE;
	buffer[1] = 0x24;	//0x10; modify by zhao
       ret = dmard06_tx_data(client, &buffer[0], 2);
	  buffer[0] = DMARD06_REG_FLITER;
	buffer[1] = 0x00;	//0x10; modify by zhao
       ret = dmard06_tx_data(client, &buffer[0], 2);
	/*     buffer[0] = DMARD06_REG_INT;
	buffer[1] = 0x24;	//0x10; modify by zhao
       ret = dmard06_tx_data(client, &buffer[0], 2);*/

	buffer[0] = DMARD06_REG_NA;
	buffer[1] = 0x00;	//0x10; modify by zhao
	ret = dmard06_tx_data(client, &buffer[0], 2);
	buffer[0] = DMARD06_REG_EVENT;
	buffer[1] = 0x2a;	//0x10; modify by zhao
	ret = dmard06_tx_data(client, &buffer[0], 2);
	buffer[0] = DMARD06_REG_Threshold;
	buffer[1] = 0x10;	//0x10; modify by zhao
	ret = dmard06_tx_data(client, &buffer[0], 2);
		buffer[0] = DMARD06_REG_Duration;
	buffer[1] = 0x10;	//0x10; modify by zhao
	ret = dmard06_tx_data(client, &buffer[0], 2);

        if(0 < rate && rate < 7)
        ret = dmard06_set_rate(client, rate);
        else
        printk("dmard06: incorrect rate value");

        buffer[0] = DMARD06_REG_INT;
	buffer[1] = 0x0c;	//0x10; modify by zhao

        ret = dmard06_tx_data(client, &buffer[0], 2);
	ret = dmard06_rx_data(client, &buffer[0], 1);

	schedule_delayed_work(&dmard06->delaywork, msecs_to_jiffies(dmard06->delay));

	PRINT_INFO("\n----------------------------dmard06_start------------------------\n");

	return ret;
}

static int dmard06_start(struct i2c_client *client, char rate)
{ 
	struct dmard06_data *dmard06 = (struct dmard06_data *)i2c_get_clientdata(client);

	if (dmard06->status == DMARD06_OPEN) {
		return 0;	   
	}
	dmard06->status = DMARD06_OPEN;
        dmard06->curr_tate = rate;
	return dmard06_start_dev(client, rate);
}

static int dmard06_close_dev(struct i2c_client *client)
{		
	char buffer[2];
	struct dmard06_data *dmard06 = (struct dmard06_data *)i2c_get_clientdata(client);
	
	cancel_delayed_work_sync(&dmard06->delaywork);

	buffer[0] = DMARD06_REG_NORMAL;
	buffer[1] = 0x07;

	return dmard06_tx_data(client, buffer, 2);
}

static int dmard06_close(struct i2c_client *client)
{
	struct dmard06_data *dmard06 = (struct dmard06_data *)i2c_get_clientdata(client);

	dmard06->status = DMARD06_CLOSE;

	return dmard06_close_dev(client);
}

static int dmard06_reset_rate(struct i2c_client *client, char rate)
{
	int ret = 0;

	PRINT_INFO("\n----------------------------dmard06_reset_rate------------------------\n");

	ret = dmard06_close_dev(client);
	ret = dmard06_start_dev(client, rate);

	return ret ;
}

static inline int dmard06_convert_to_int(char value)
{
	int result;

	if (value < DMARD06_BOUNDARY) {
	   result = value * DMARD06_GRAVITY_STEP;
	} else {
	   result = ~(((~value & 0x3f) + 1)* DMARD06_GRAVITY_STEP) + 1;
	}

	return result;
}



static void dmard06_report_value(struct i2c_client *client, struct dmard06_axis *axis)
{
	struct dmard06_data *dmard06 = i2c_get_clientdata(client);

	/* Report acceleration sensor information */
	/*
	spec_axis->x = axis->x;
	spec_axis->y = axis->y;
	spec_axis->z = axis->z;
	*/

        //printk("dmard06 devid:%x %x %x %x\n", 0, axis->x, axis->y, axis->z);

/*
	input_report_abs(dmard06->input_dev, ABS_X, axis->x);
	input_report_abs(dmard06->input_dev, ABS_Y, axis->y);
	input_report_abs(dmard06->input_dev, ABS_Z, axis->z);
        input_sync(dmard06->input_dev);
*/
        input_event(dmard06->input_dev, EV_ABS, ABS_X, axis->x);
        input_event(dmard06->input_dev, EV_ABS, ABS_Y, axis->y);
        input_event(dmard06->input_dev, EV_ABS, ABS_Z, axis->z);
        input_event(dmard06->input_dev, EV_SYN, 0, 0);
	
}

#define RawDataLength 4
int RawDataNum;
int Xaverage, Yaverage, Zaverage;

static int dmard06_get_data(struct i2c_client *client)
{
	signed char buffer[3];
	int ret;
	int x,y,z;
	struct dmard06_axis axis;
        struct gsensor_platform_data *pdata = client->dev.platform_data;

	do {
	memset(buffer, 0, 3);
		buffer[0] = DMARD06_REG_X_OUT;
		ret = dmard06_rx_data(client, &buffer[0], 3);
		if (ret < 0)
			return ret;
	} while (0);

        x =dmard06_convert_to_int(buffer[0]>>1);
        y =dmard06_convert_to_int(buffer[1]>>1);
        z =dmard06_convert_to_int(buffer[2]>>1);
	
	axis.x = x;
	axis.y = y;
	axis.z = z;
	
	if (pdata->swap_xyz)
        {
        axis.x = (pdata->orientation[0]) * x + (pdata->orientation[1]) * y + (pdata->orientation[2]) * z;
        axis.y = (pdata->orientation[3]) * x + (pdata->orientation[4]) * y + (pdata->orientation[5]) * z;
        axis.z = (pdata->orientation[6]) * x + (pdata->orientation[7]) * y + (pdata->orientation[8]) * z;
        }

        if(pdata->swap_xy)
        {
        x = -x;
        swap(x,y);
        }

	dmard06_report_value(client, &axis);

	return 0;
}

static int dmard06_open(struct inode *inode, struct file *file)
{
	return 0;//nonseekable_open(inode, file);
}

static int dmard06_release(struct inode *inode, struct file *file)
{
	return 0;
}

static long dmard06_ioctl(struct file *file, unsigned int cmd,unsigned long arg)
{


	void __user *argp = (void __user *)arg;
	char msg[RBUFF_SIZE + 1];
	int ret = -1;
	char rate;
	struct i2c_client *client = container_of(dmard06_device.parent, struct i2c_client, dev);


        printk("dmard06_ioctl cmd:%x\n", cmd);
	switch (cmd) {
	case MMA_IOCTL_APP_SET_RATE:
		if (copy_from_user(&rate, argp, sizeof(rate)))
			return -EFAULT;
		break;
	default:
		break;
	}

	switch (cmd) {
	case MMA_IOCTL_START:

		ret = dmard06_start(client, DMARD06_RATE_32);
		if (ret < 0)
			return ret;
		break;
	case MMA_IOCTL_CLOSE:
		ret = dmard06_close(client);
		if (ret < 0)
			return ret;
		break;
	case MMA_IOCTL_APP_SET_RATE:
		ret = dmard06_reset_rate(client, rate);
                printk("dmard06 set rate to %x\n", rate);
		if (ret < 0)
			return ret;
		break;

	default:
		return -ENOTTY;
	}

	switch (cmd) {
	case MMA_IOCTL_GETDATA:
		if (copy_to_user(argp, &msg, sizeof(msg)))
			return -EFAULT;
		break;
	default:
		break;
	}

	return 0;
}


static char dmard06_get_devid(struct i2c_client *client)
{
        unsigned char buffer[2];
        buffer[0] = SW_RESET;
	dmard06_rx_data(client,&buffer[0],1);

        buffer[0] = DMARD06_REG_WHO_AM_I;
        dmard06_rx_data(client,&buffer[0],1);
	printk("dmard06 devid:%x\n",buffer[0]);

	   return buffer[0];
}

static void  dmard06_delaywork_func(struct work_struct *work)
{
	struct dmard06_data *data = container_of(work, struct dmard06_data, delaywork.work);
	
	if (dmard06_get_data(data->client) < 0) 
		PRINT_INFO(KERN_ERR "DMARD06 mma_work_func: Get data failed\n");
	
	//printk("working!!!!\n");
	
	if (!delayed_work_pending(&data->delaywork))
		schedule_delayed_work(&data->delaywork, msecs_to_jiffies(data->delay));
	else
		printk(KERN_ERR "errror? problem?\n");
}


static struct file_operations dmard06_fops = {
	.owner			= THIS_MODULE,
	.open			= dmard06_open,
	.release		= dmard06_release,
	.unlocked_ioctl	        = dmard06_ioctl,
};

static struct miscdevice dmard06_device = {
	.minor	= MISC_DYNAMIC_MINOR,
	.name	= "mma8452_daemon",
	.fops	= &dmard06_fops,
};

static int dmard06_remove(struct i2c_client *client)
{
	struct dmard06_data *dmard06 = i2c_get_clientdata(client);

	misc_deregister(&dmard06_device);
	input_unregister_device(dmard06->input_dev);
	input_free_device(dmard06->input_dev);
	free_irq(client->irq, dmard06);
	kfree(dmard06); 
#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&dmard06_early_suspend);
#endif     
	this_client = NULL;
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void dmard06_suspend(struct early_suspend *h)
{
	struct i2c_client *client = container_of(dmard06_device.parent, struct i2c_client, dev);
	PRINT_INFO("Gsensor dmard06 enter suspend\n");
	dmard06_close_dev(client);
}

static void dmard06_resume(struct early_suspend *h)
{
	struct i2c_client *client = container_of(dmard06_device.parent, struct i2c_client, dev);
	struct dmard06_data *dmard06 = (struct dmard06_data *)i2c_get_clientdata(client);
	PRINT_INFO("Gsensor dmard06 resume!!\n");
	dmard06_start_dev(client, dmard06->curr_tate);
}

#endif

static const struct i2c_device_id dmard06_id[] = {
	{"gs_dmard06", 0},
	{ }
};

static struct i2c_driver dmard06_driver = {
	.driver = {
		.name = "gs_dmard06",
	},
	.id_table 	= dmard06_id,
	.probe		= dmard06_probe,           
	.remove		= __devexit_p(dmard06_remove),
#ifndef CONFIG_HAS_EARLYSUSPEND	
	.suspend	= &dmard06_suspend,
	.resume		= &dmard06_resume,
#endif	
};

static int dmard06_init_client(struct i2c_client *client)
{
	struct dmard06_data *dmard06;
	int ret = 0;
	dmard06 = i2c_get_clientdata(client);
	
	return ret;
}

static int dmard06_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct dmard06_data *dmard06;
	int err = -1;
	char devid;
	Xaverage = Yaverage = Zaverage = RawDataNum = 0;

	dmard06 = kzalloc(sizeof(struct dmard06_data), GFP_KERNEL);
	if (!dmard06) {
		PRINT_INFO("[dmard06]:alloc data failed.\n");
		err = -ENOMEM;
		goto exit_alloc_data_failed;
	}

	dmard06->client = client;
	dmard06->delay = DEF_DELAY;
	i2c_set_clientdata(client, dmard06);

	this_client = client;
       devid = dmard06_get_devid(client);
	/*
	if (DMARD06_DEVID != devid) {
		pr_info("DMARD06: invalid devid\n");
		goto exit_invalid_devid;
	}
	*/

	err = dmard06_init_client(client);
	if (err < 0) {
		PRINT_INFO(KERN_ERR
			   "dmard06_probe: dmard06_init_client failed\n");
		goto exit_request_gpio_irq_failed;
	}



	dmard06->input_dev = input_allocate_device();
	if (!dmard06->input_dev) {
		err = -ENOMEM;
		PRINT_INFO(KERN_ERR
			   "dmard06_probe: Failed to allocate input device\n");
		goto exit_input_allocate_device_failed;
	}

	set_bit(EV_ABS, dmard06->input_dev->evbit);

	/* x-axis acceleration */
	input_set_abs_params(dmard06->input_dev, ABS_X, -RANGE, RANGE, 0, 0);
	/* y-axis acceleration */
	input_set_abs_params(dmard06->input_dev, ABS_Y, -RANGE, RANGE, 0, 0);
	/* z-axis acceleration */
	input_set_abs_params(dmard06->input_dev, ABS_Z, -RANGE, RANGE, 0, 0);

	dmard06->input_dev->name = "gsensor";
	dmard06->input_dev->dev.parent = &client->dev;

	err = input_register_device(dmard06->input_dev);
	if (err < 0) {
		PRINT_INFO(KERN_ERR
			   "dmard06_probe: Unable to register input device: %s\n",
			   dmard06->input_dev->name);
		goto exit_input_register_device_failed;
	}

	dmard06_device.parent = &client->dev;
	err = misc_register(&dmard06_device);
	if (err < 0) {
		PRINT_INFO(KERN_ERR
			   "dmard06_probe: mmad_device register failed\n");
		goto exit_misc_device_register_dmard06_device_failed;
	}
	err = gsensor_sysfs_init();
	if (err < 0) {
		PRINT_INFO(KERN_ERR
			"dmard06_probe: gsensor sysfs init failed\n");
		goto exit_gsensor_sysfs_init_failed;
	}

#ifdef CONFIG_HAS_EARLYSUSPEND
    dmard06_early_suspend.suspend	= dmard06_suspend;
    dmard06_early_suspend.resume		= dmard06_resume;
    dmard06_early_suspend.level		= 0x2;
    register_early_suspend(&dmard06_early_suspend);
#endif
	
	
	INIT_DELAYED_WORK(&dmard06->delaywork, dmard06_delaywork_func);
	//schedule_delayed_work(&dmard06->delaywork, msecs_to_jiffies(1000));

	PRINT_INFO(KERN_INFO "dmard06 probe ok\n");
	dmard06->status = -1;
//	dmard06_start(client,0);

	return 0;

exit_gsensor_sysfs_init_failed:
	misc_deregister(&dmard06_device);
exit_misc_device_register_dmard06_device_failed:
	input_unregister_device(dmard06->input_dev);
exit_input_register_device_failed:
	input_free_device(dmard06->input_dev);
exit_input_allocate_device_failed:
	free_irq(client->irq, dmard06);
exit_request_gpio_irq_failed:
	cancel_delayed_work_sync(&dmard06->delaywork);
exit_invalid_devid:
	kfree(dmard06); 
exit_alloc_data_failed:
	printk("%s error\n",__FUNCTION__);
	return err;
}

static int __init dmard06_i2c_init(void)
{
	return i2c_add_driver(&dmard06_driver);
}

static void __exit dmard06_i2c_exit(void)
{
	i2c_del_driver(&dmard06_driver);
}

module_init(dmard06_i2c_init);
module_exit(dmard06_i2c_exit);

MODULE_DESCRIPTION ("STMicroelectronics gsensor driver");
MODULE_AUTHOR("LB<libing@rock-chips.com>");
MODULE_LICENSE("GPL");
