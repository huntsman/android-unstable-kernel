/****************************************************************************************
*17.05.14
 *****************************************************************************************/
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/miscdevice.h>
#include <linux/gpio.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/freezer.h>
#include <mach/gpio.h>
#include <mach/board.h> 
#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

#include "dmard08.h"

#if 0
#define PRINT_INFO(x...) printk(x)
#else
#define PRINT_INFO(x...)
#endif

#define DMARD08_SPEED		200 * 1000

#define DEF_DELAY	30

static struct miscdevice dmard08_device;
static struct kobject *android_gsensor_kobj;
static const char* vendor = "DMARD08 GS-SENSOR";
#ifdef CONFIG_HAS_EARLYSUSPEND
static struct early_suspend dmard08_early_suspend;
#endif
static int  dmard08_probe(struct i2c_client *client, const struct i2c_device_id *id);
static DECLARE_WAIT_QUEUE_HEAD(data_ready_wq);

/* AKM HW info */
static ssize_t gsensor_vendor_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	ssize_t ret = 0;

	 sprintf(buf, "%s.\n", vendor);
	ret = strlen(buf) + 1;

	return ret;
}

static DEVICE_ATTR(vendor, 0444, gsensor_vendor_show, NULL);

static int gsensor_sysfs_init(void)
{
	int ret ;

	android_gsensor_kobj = kobject_create_and_add("android_gsensor", NULL);
	if (android_gsensor_kobj == NULL) {
		PRINT_INFO(KERN_ERR "DMARD08 gsensor_sysfs_init: subsystem_register failed\n");
		ret = -ENOMEM;
		goto err;
	}

	ret = sysfs_create_file(android_gsensor_kobj, &dev_attr_vendor.attr);
	if (ret) {
		PRINT_INFO(KERN_ERR "DMARD08 gsensor_sysfs_init: sysfs_create_group failed\n");
		goto err4;
	}

	return 0 ;
err4:
	kobject_del(android_gsensor_kobj);
err:
	return ret ;
}

static int dmard08_rx_data(struct i2c_client *client, char *rxData, int length)
{
	int liRet = 0;
	char lcReg = rxData[0];
	liRet = i2c_master_reg8_recv(client, lcReg, rxData, length, DMARD08_SPEED);
	
	return (liRet > 0)? 0 : liRet;
}

static int dmard08_tx_data(struct i2c_client *client, char *txData, int length)
{
	int liRet	= 0;
	char lcReg	= txData[0];
	liRet = i2c_master_reg8_send(client, lcReg, &txData[1], length-1, DMARD08_SPEED);
	
	return (liRet > 0)? 0 : liRet;
}

static void dmard08_start(struct i2c_client *client)
{ 
	struct dmard08_data *dmard08 = (struct dmard08_data *)i2c_get_clientdata(client);
	schedule_delayed_work(&dmard08->delaywork, msecs_to_jiffies(dmard08->delay));
}

static void dmard08_close(struct i2c_client *client)
{
	struct dmard08_data *dmard08 = (struct dmard08_data *)i2c_get_clientdata(client);
        cancel_delayed_work_sync(&dmard08->delaywork);
}

static void dmard08_report_value(struct i2c_client *client, struct dmard08_axis *axis)
{
	struct dmard08_data *dmard08 = i2c_get_clientdata(client);

        input_event(dmard08->input_dev, EV_ABS, ABS_X, axis->x);
        input_event(dmard08->input_dev, EV_ABS, ABS_Y, axis->y);
        input_event(dmard08->input_dev, EV_ABS, ABS_Z, axis->z);
        input_event(dmard08->input_dev, EV_SYN, 0, 0);
}

static inline s64 dmard08_convert_to_int(const char High_Value, const char Low_Value)
{
	s64 Result;
	
 	Result = ((long) High_Value << 3) | Low_Value;
	
    if (Result < DMARD08_BOUNDARY)
	{
       Result = Result * DMARD08_GRAVITY_STEP;
    } 
	else
	{
       Result = ~(((~Result & 0x3FF) + 1)* DMARD08_GRAVITY_STEP) + 1;
    }

    return Result;
}

static int dmard08_get_data(struct i2c_client *client)
{
	signed char buffer[6];
	int x,y,z;
        int ret = 0;
	struct dmard08_axis axis;
        struct gsensor_platform_data *pdata = client->dev.platform_data;

	do {
	memset(buffer, 0, 6);
		buffer[0] = DMARD08_REG_X_OUT;
		ret = dmard08_rx_data(client, &buffer[0], 6);
		if (ret < 0)
			return ret;
	} while (0);

	x = dmard08_convert_to_int(buffer[0],buffer[1]);
	y = dmard08_convert_to_int(buffer[2],buffer[3]);
	z = dmard08_convert_to_int(buffer[4],buffer[5]);
	
	axis.x = x;
	axis.y = y;
	axis.z = z;
	
	if (pdata->swap_xyz)
        {
        axis.x = (pdata->orientation[0]) * x + (pdata->orientation[1]) * y + (pdata->orientation[2]) * z;
        axis.y = (pdata->orientation[3]) * x + (pdata->orientation[4]) * y + (pdata->orientation[5]) * z;
        axis.z = (pdata->orientation[6]) * x + (pdata->orientation[7]) * y + (pdata->orientation[8]) * z;
        }

        if(pdata->swap_xy)
        {
        x = -x;
        swap(x,y);
        }

	dmard08_report_value(client, &axis);

	return 0;
}

static int dmard08_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int dmard08_release(struct inode *inode, struct file *file)
{
	return 0;
}

static long dmard08_ioctl(struct file *file, unsigned int cmd,unsigned long arg)
{
	struct i2c_client *client = container_of(dmard08_device.parent, struct i2c_client, dev);
        PRINT_INFO("dmard08_ioctl cmd:%x\n", cmd);

	switch (cmd) {
	case MMA_IOCTL_START:
		dmard08_start(client);
		break;
	case MMA_IOCTL_CLOSE:
		dmard08_close(client);
		break;
	default:
		return -ENOTTY;
	}

	return 0;
}

static int dmard08_checkdevice (struct i2c_client *client)
{
	unsigned char databuf[2];    

	databuf[0] = 0x08;    
	dmard08_rx_data( client, &databuf[0], 1);
	if( databuf[0] == 0x00)
	{
		databuf[0] = 0x09;
		dmard08_rx_data( client, &databuf[0], 1);	
		if( databuf[0] == 0x00)
		{
			databuf[0] = 0x0a;
			dmard08_rx_data( client, &databuf[0], 1);	
			if( databuf[0] == 0x88)
			{
				databuf[0] = 0x0b;
				dmard08_rx_data( client, &databuf[0], 1);	
				if( databuf[0] == 0x08)
				{

				}
				else
				{
					printk("DMARD08_CheckDeviceID1 %d failt!\n ", databuf[0]);
					return -1;
				}
			}
			else
			{
				printk("DMARD08_CheckDeviceID2 %d failt!\n ", databuf[0]);
				return -1;
			}
		}
		else
		{
			printk("DMARD08_CheckDeviceID3 %d failt!\n ", databuf[0]);
			return -1;
		}
	}
	else
	{
		printk("DMARD08_CheckDeviceID4 %d failt!\n ", databuf[0]);
		return -1;
	}
	printk("DMARD08_CheckDeviceID %d success!\n ", databuf[0]);
	return 0;
}

static void  dmard08_delaywork_func(struct work_struct *work)
{
	struct dmard08_data *data = container_of(work, struct dmard08_data, delaywork.work);
	
	if (dmard08_get_data(data->client) < 0) 
		PRINT_INFO(KERN_ERR "DMARD08_delaywork_func: Get data failed\n");

	
	if (!delayed_work_pending(&data->delaywork))
		schedule_delayed_work(&data->delaywork, msecs_to_jiffies(data->delay));
	else
		PRINT_INFO(KERN_ERR "DMARD08: errror? problem?\n");
}


static struct file_operations dmard08_fops = {
	.owner			= THIS_MODULE,
	.open			= dmard08_open,
	.release		= dmard08_release,
	.unlocked_ioctl	        = dmard08_ioctl,
};

static struct miscdevice dmard08_device = {
	.minor	= MISC_DYNAMIC_MINOR,
	.name	= "mma8452_daemon",
	.fops	= &dmard08_fops,
};

static int dmard08_remove(struct i2c_client *client)
{
	struct dmard08_data *dmard08 = i2c_get_clientdata(client);

	misc_deregister(&dmard08_device);
	input_unregister_device(dmard08->input_dev);
	input_free_device(dmard08->input_dev);
	free_irq(client->irq, dmard08);
	kfree(dmard08); 
#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&dmard08_early_suspend);
#endif     
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void dmard08_suspend(struct early_suspend *h)
{
	struct i2c_client *client = container_of(dmard08_device.parent, struct i2c_client, dev);
	PRINT_INFO("Gsensor dmard08 enter suspend\n");
	dmard08_close(client);
}

static void dmard08_resume(struct early_suspend *h)
{
	struct i2c_client *client = container_of(dmard08_device.parent, struct i2c_client, dev);
	PRINT_INFO("Gsensor dmard08 resume!!\n");
	dmard08_start(client);
}

#endif

static const struct i2c_device_id dmard08_id[] = {
	{"gs_dmard08", 0},
	{ }
};

static struct i2c_driver dmard08_driver = {
	.driver = {
		.name = "gs_dmard08",
	},
	.id_table 	= dmard08_id,
	.probe		= dmard08_probe,           
	.remove		= __devexit_p(dmard08_remove),
#ifndef CONFIG_HAS_EARLYSUSPEND	
	.suspend	= &dmard08_suspend,  
	.resume		= &dmard08_resume,
#endif	
};

static int dmard08_init_client(struct i2c_client *client)
{
	int ret = 0;
	return ret;
}

static int dmard08_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct dmard08_data *dmard08;
	int err = -1;

	dmard08 = kzalloc(sizeof(struct dmard08_data), GFP_KERNEL);
	if (!dmard08) {
		PRINT_INFO("dmard08:alloc data failed.\n");
		err = -ENOMEM;
		goto exit_alloc_data_failed;
	}

	dmard08->client = client;
	dmard08->delay = DEF_DELAY;
	i2c_set_clientdata(client, dmard08);

	err = dmard08_checkdevice(client);
	if (err < 0) {
		goto exit_invalid_devid;
	}

	INIT_DELAYED_WORK(&dmard08->delaywork, dmard08_delaywork_func);

	err = dmard08_init_client(client);
	if (err < 0) {
		PRINT_INFO(KERN_ERR "dmard08_probe: dmard08_init_client failed\n");
		goto exit_request_gpio_irq_failed;
	}

	dmard08->input_dev = input_allocate_device();
	if (!dmard08->input_dev) {
		err = -ENOMEM;
		PRINT_INFO(KERN_ERR "dmard08_probe: Failed to allocate input device\n");
		goto exit_input_allocate_device_failed;
	}

	set_bit(EV_ABS, dmard08->input_dev->evbit);

	/* x-axis acceleration */
	input_set_abs_params(dmard08->input_dev, ABS_X, -RANGE, RANGE, 0, 0);
	/* y-axis acceleration */
	input_set_abs_params(dmard08->input_dev, ABS_Y, -RANGE, RANGE, 0, 0);
	/* z-axis acceleration */
	input_set_abs_params(dmard08->input_dev, ABS_Z, -RANGE, RANGE, 0, 0);

	dmard08->input_dev->name = "gsensor";
	dmard08->input_dev->dev.parent = &client->dev;

	err = input_register_device(dmard08->input_dev);
	if (err < 0) {
		PRINT_INFO(KERN_ERR "dmard08_probe: Unable to register input device: %s\n", dmard08->input_dev->name);
		goto exit_input_register_device_failed;
	}

	dmard08_device.parent = &client->dev;
	err = misc_register(&dmard08_device);
	if (err < 0) {
		PRINT_INFO(KERN_ERR "dmard08_probe: mmad_device register failed\n");
		goto exit_misc_device_register_dmard08_device_failed;
	}
	err = gsensor_sysfs_init();
	if (err < 0) {
		PRINT_INFO(KERN_ERR "dmard08_probe: gsensor sysfs init failed\n");
		goto exit_gsensor_sysfs_init_failed;
	}

#ifdef CONFIG_HAS_EARLYSUSPEND
    dmard08_early_suspend.suspend	= dmard08_suspend;
    dmard08_early_suspend.resume		= dmard08_resume;
    dmard08_early_suspend.level		= 0x2;
    register_early_suspend(&dmard08_early_suspend);
#endif

	printk(KERN_INFO "dmard08 probe ok\n");
	return 0;

exit_gsensor_sysfs_init_failed:
	misc_deregister(&dmard08_device);
exit_misc_device_register_dmard08_device_failed:
	input_unregister_device(dmard08->input_dev);
exit_input_register_device_failed:
	input_free_device(dmard08->input_dev);
exit_input_allocate_device_failed:
	free_irq(client->irq, dmard08);
exit_request_gpio_irq_failed:
	cancel_delayed_work_sync(&dmard08->delaywork);
exit_invalid_devid:
	kfree(dmard08); 
exit_alloc_data_failed:
	printk("%s error\n",__FUNCTION__);
	return err;
}

static int __init dmard08_i2c_init(void)
{
	return i2c_add_driver(&dmard08_driver);
}

static void __exit dmard08_i2c_exit(void)
{
	i2c_del_driver(&dmard08_driver);
}

module_init(dmard08_i2c_init);
module_exit(dmard08_i2c_exit);

MODULE_DESCRIPTION ("DMARD08 I2C driver");
MODULE_AUTHOR("HuntsMan");
MODULE_LICENSE("GPL");